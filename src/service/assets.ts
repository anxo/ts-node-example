import { Asset } from "../types"

const store = new Map<string, Asset>()

const example: Asset = {
  "id": "91cb92d0-b32e-4f97-8ad1-737a3fda8546",
  "address": "Demo Street, 123",
  "description": "Great offer...",
  "status": "DRAFT"
}

export function get(id: string): Asset | undefined {
  return store.get(id)
}

export function list(): Asset[] {
  return Array.from(store.values())
}

export function save(id: string, asset: Asset): void {
  store.set(id, asset)
}

export function remove(id: string): void {
  store.delete(id)
}
