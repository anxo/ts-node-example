export type AssetStatus = 'DRAFT' | 'PUBLISHED' | 'DELETED'

export interface Asset {
  id: string
  address: string
  description: string
  status?: AssetStatus
}

/*
export type Asset = {
  id: string
  //...
}
*/
