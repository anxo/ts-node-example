import { Request, RequestHandler, Response } from 'express'
import { v4 as uuidv4 } from 'uuid'
import * as assetRepository from '../service/assets'
import { Asset } from '../types'

export const list: RequestHandler = (req, res) => {
  const assets = assetRepository.list()
  res.send(assets)
}

export function get(req: Request, res: Response) {
  const id = req.params.id
  const asset = assetRepository.get(id)
  if (!asset) {
    res.status(404).send({ error: 'Not found' })
    return
  }
  res.send(asset)
}

export function create(req: Request, res: Response) {
  if (!req.body) {
    res.status(400).send({ error: 'Body is required' })
    return
  }
  const { address, description } = req.body
  if (!address || !description) {
    res.status(400).send({ error: 'Missing a required field' })
    return
  }
  const id = uuidv4()
  const asset: Asset = { id, address, description, status: 'DRAFT' }
  assetRepository.save(id, asset)
  res.send(asset)
}
