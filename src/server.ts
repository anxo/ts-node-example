import express from 'express'
import bodyParser from 'body-parser'
import * as assetController from './controller/assets'
const app = express()
const port: number = 3000

app.use(bodyParser.json())

app.get('/', (req, res) => res.send('Server is running...'))
app.get('/assets', assetController.list)
app.get('/assets/:id', assetController.get)
app.post('/assets', assetController.create)

app.listen(port, () => {
  console.log(`Server listening on port ${port}`)
})
