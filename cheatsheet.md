# Chuleta de tipado

## Sintaxis

Los tipos se escriben de las siguientes formas:

```ts
const a: number = 123

let b: number

function demo(arg1: number, arg2: string): boolean { /* ... */ }

const demoArrow: SomeCustomFunctionType = () => { /* ... */ }
```

## Tipos básicos

Son los siguientes:

```ts
const a: number = 123.456
const b: string = 'abcd'
const c: boolean = true
```

## Tipos especiales

Además de los tipos normales, existen estos:

- `any`: permite cualquier valor y desactiva la verificación para esta variable
- `void`: no permite ningún valor; se usa para indicar que una función nunca devuelve nada

## Arrays

Se escriben de las siguientes formas:

```ts
const a: number[] = [1, 2, 3]
const b: Array<string> = ['a', 'b', 'c']
```

Los tipos base pueden ser cualquier tipo, incluyendo uniones para permitir elementos de tipos diferentes.

Si se quiere indicar la cantidad y tipo específico de elementos, se puede hacer así:

```ts
const a: [number, string] = [1, 'a'] // No permitiría ni más ni menos parámetros
```

## Objetos

Los objetos se definen por los campos que contienen:

```ts
function demo(obj: { id: number, title: string }) { /* ... */ }
```

Aunque lo normal es definir un interfaz o tipo y usar eso:

```ts
interface SomeEntity {
  id: number
  title: string
  visible: boolean
}

// O bien

type SomeEntity = {
  id: number
  title: string
  visible: boolean
}

// Uso:

const entity: SomeEntity = { id: 123, title: 'Demo', visible: true }
function demo(e: SomeEntity): void { /* ... */ }
```

Los tipos e interfaces se pueden exportar e importar, y es habitual definir
los que se usan en muchos sitios en un fichero aparte por comodidad.

## Opcional

Cualquier tipo, en cualquier contexto, se puede marcar como opcional mediante un `?`:

```ts
interface SomeEntity {
  id: number
  title: string
  visible?: boolean // Se puede omitir, pero si se indica tiene que ser boolean
}

function demo(optionalParam?: string) { /* ... */ }
```

## Union

Es posible definir tipos que sean la unión de varios tipos (o valores) de los anteriores:

```ts
const port: number | string = 3000

// Es frecuente definirlo como un type para hacerlo más legible:

type Status = 'BUSY' | 'IDLE' | 'UNKNOWN'
const loadStatus: Status = 'IDLE'
```

## Genéricos

Algunos elementos se definen a partir de los tipos que reciben o devuelven en sus funciones.
En estos casos resulta útil crear y utilizar tipos genéricos, de forma que se pueda indicar
con qué tipos concretos trabajará en un caso específico.

```ts
const myMap = new Map<string, boolean>() // Los key serán string, y los valores boolean

myMap.set('demo', true) // Los parámetros tienen que ser de los tipos indicados arriba
myMap.get('demo') // Devuelve el tipo del valor (boolean en este caso)
```

Nos queda pendiente ver cómo definir nuestros propios tipos genéricos.

## Dependencias con tipos

Algunas dependencias incluyen información de tipos en el propio paquete, pero con otras
es necesario instalar esta información aparte. El Visual Studio nos lo indicará en el import
y nos sugerirá ejecutar un comando como:

```sh
npm install @types/express
```

Cambiando `express` por el nombre de la dependencia en cuestión.
