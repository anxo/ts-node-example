# TS Node Example

Un ejemplo de aplicación Node básica, usando nodemon.

En los siguientes pasos del desarrollo, la pasaremos a Typescript, a modo de
ejemplo para familiarizarnos con los pasos necesarios en un proyecto típico de
este tipo.

## Versiones

Cada "paso" del proceso se corresponde con un commit, para poder comparar y
saltar entre versiones.

## Uso

En todas las versiones los pasos son los habituales:

```sh
npm install
npm start
```

## Enlaces

Puedes ver un resumen de tipado en [cheatsheet.md](cheatsheet.md).

También puede interesarte la [presentación](https://docs.google.com/presentation/d/1rOpJYDE6bO4Gmqh2iBZgohlYZzovx16-NoITR8U-Jjc/edit?usp=sharing) usada al principio de la charla.

Para aprender más sobre el tipado en React, os recomiendo esta página: [react-typescript-cheatsheet](https://react-typescript-cheatsheet.netlify.app/docs/basic/getting-started/class_components).
